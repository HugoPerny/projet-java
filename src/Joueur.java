import java.io.Serializable;
import java.util.ArrayList;

public class Joueur implements Serializable {

	private ArrayList<Carte> main;
	private int age;
	private String nom;
	private int score;
	
	/**
	 * Creation d'un joueur
	 *
	 * @param n
	 * 			nom du joueur
	 * @param a
	 *			age du joueur
	 */
	public Joueur(String n, int a){
		this.age=a;
		this.nom=n;
		this.main=new ArrayList<Carte>();
		this.score=1;
	}
	
	/**
	 * Retourne l'age d'un joueur
	 *
	 * @return age du joueur sous forme d'un entier
	 */
	public int getAge(){
		return this.age;
	}

	/**
	 * Ajoute une carte à la main d'un joueur
	 *
	 * @param c
	 * 			carte à ajouter à la main d'un joueur
	 */
	public void addCarte(Carte c){
		main.add(c);
	}

	/**
	 * Enleve une carte de la main d'un joueur
	 *
	 * @param c
	 * 			carte à enlever de la main d'un joueur
	 */
	public void joue(Carte c){
		main.remove(c);
	}

	/**
	 * Retourne le nombre de cartes dans la main d'un joueur
	 *
	 * @return nombre de cartes dans la main d'un joueur sous forme d'un entier
	 */
	public int nombreDeCartes(){
		return this.main.size();
	}

	/**
	 * Retourne les cartes de la main d'un joueur
	 *
	 * @return carte dans la main d'un joueur, sous la forme d'une liste de carte
	 */
	public ArrayList<Carte> getCartes(){
		return main;
	}

	/**
	 * Retourne le nom d'un joueur
	 *
	 * @return nom d'un joueur sous la forme d'une chaine de caractères
	 */	
	public String getNom() {
		return nom;
	}

	/**
	 * Incrémente le compteur de tour d'un joueur
	 */
	public void tourPassé() {
		score+=1;
	}

	/**
	 * Retourne le nombre de tour passé d'un joueur
	 *
	 * @return le nombre de tour joué d'un joueur sous la forme d'un entier
	 */
	public int getScore() {
		return score;
	}
	
}

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Plateau implements Serializable {
	
	public static void main(String[] args) {
		new Plateau();
		
	}

	public final File scoreboard=new File("data/files/scores");
	private final File savefile=new File("data/files/savefile");
	private String jeu="timeline";
	private ArrayList<Carte> aireDeJeu;
	private ArrayList<Carte> paquet;
	private ArrayList<Joueur> joueurs;
	private ArrayList<Joueur> vainqueurs;
	private Fenetre fenetre;
	

	/**
	 * Crée un nouveau jeu
	 */
	public Plateau(){
		this.aireDeJeu = new ArrayList<Carte>();
		this.paquet = new ArrayList<Carte>();
		//génération des cartes
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File("data/"+jeu+"/"+jeu+".csv")));
			String line;
			//on dégage la premièreligne
			br.readLine();
			while((line = br.readLine()) != null){
				Carte card = new Carte(jeu,line);
				paquet.add(card);
			}
			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("Le fichier n'existe pas");
		} catch (IOException e) {
			System.out.println("Un fichier n'existe pas");
		}
		vainqueurs = new ArrayList<Joueur>();
		
		//inscriptions des joueurs
		
		new FenetreInitialisation(this);
	}

	/**
	 * Initialise la liste des participants
	 *
	 * @param j
	 * 			liste des joueurs participants
	 */
	public void initilisationJoueurs(ArrayList<Joueur> j) {
		joueurs=j;
		start();
	}

	/**
	 * Détermine l'ordre de jeu
	 */
	private void ordreDeJeu(){
		Joueur min = joueurs.get(0);
		
		for(int i=1;i<joueurs.size();i++){
			if(joueurs.get(i).getAge()<min.getAge())
				min = joueurs.get(i);
		}
		
		int i=0;
		
		while(joueurs.get(i)!=min){
			Joueur temp = joueurs.get(i);
			joueurs.remove(i);
			joueurs.add(temp);
		}
	}

	/**
	 * Distribue les cartes initiales à chaque 
	 */
	private void mainDepart(){
		int nb_cartes;
		if(joueurs.size() <= 3)
			nb_cartes=6;
		else if(joueurs.size() <= 5)
			nb_cartes=5;
		else
			nb_cartes=4;
		for(Joueur j : joueurs){
			for(int i=0;i<nb_cartes;i++){
				j.addCarte(pioche());
			}
		}
	}

	/**
	 * Lance "officiellement" le jeu
	 */
	private void start(){
		ordreDeJeu();
		//initialisation des mains
		mainDepart();
		
		//choix de la première carte
		aireDeJeu.add(pioche());
		this.fenetre = new Fenetre(jeu, this);
	}

	/**
	 * Termine le tour d'un joueur, détermine s'il a terminé ou non et passe la main au joueur suivant
	 */
	public void tourSuivant() {
		Joueur j = joueurs.get(0);
		j.tourPassé();
		if(j.nombreDeCartes() == 0) {
			vainqueurs.add(j);
			ArrayList<Joueur> enlice = new ArrayList<Joueur>();
			int i = 1;
			while(i<joueurs.size() && joueurs.get(i).getScore()<j.getScore()) {
				enlice.add(joueurs.get(i));
				i++;
			}
			if(enlice.size()>0) {
				joueurs=enlice;
			} else if (vainqueurs.size() > 1) {
				joueurs=new ArrayList<Joueur>(vainqueurs);
				vainqueurs.clear();
				suddenDeath();
			} else {
				ajoutScoreboard(j);
				fenetre.finJeu(j.getNom());		
			}
		} else {
			joueurs.remove(0);
			if(vainqueurs.isEmpty())
				joueurs.add(j);
			if(joueurs.isEmpty()) {
				if(vainqueurs.size()>1) {
					joueurs=vainqueurs;
					suddenDeath();
				} else {
					ajoutScoreboard(vainqueurs.get(0));
					fenetre.finJeu(vainqueurs.get(0).getNom());
				}
				
			}
		}
		this.fenetre.updateFrame(joueurs.get(0).getNom());
	}

	/**
	 * Place une carte à un endroit spécifique de l'aire de jeu
	 *
	 * @param c
	 * 			carte à placer
	 * @param place
	 * 				emplacement de la carte à placer
	 * @return boolean pour dire si l'opération à réussie ou non
	 */
	public boolean placer(String c, int place) {
		int i = 0;
		Carte carte = joueurs.get(0).getCartes().get(i);
		while(carte.getNom() != c) {
			i++;
			carte = joueurs.get(0).getCartes().get(i);
		}
		joueurs.get(0).joue(carte);
		if((place == 1 && aireDeJeu.get(place-1).getDate() > carte.getDate()) || (place == aireDeJeu.size()+1 && aireDeJeu.get(place-2).getDate() < carte.getDate()) || (place > 1 && place < aireDeJeu.size()+1 && aireDeJeu.get(place-2).getDate() < carte.getDate() && carte.getDate() < aireDeJeu.get(place-1).getDate())){
			this.aireDeJeu.add(place-1,carte);
			return true;
		} else {
			joueurs.get(0).addCarte(pioche());
			return false;
		}
	}

	/**
	 * Choisi une carte aléatoire dans le paquet et l'enlève
	 *
	 * @return carte retirée du paquet
	 */
	private Carte pioche(){
		Random randomizer = new Random();
		Carte c = paquet.get(randomizer.nextInt(paquet.size()));
		paquet.remove(c);
		return c;
	}

	/**
	 * Retourne les cartes de l'aire de jeu
	 *
	 * @return carte de l'aire de jeu sous la forme d'une liste de cartes
	 */
	public ArrayList<Carte> getAireDeJeu(){
		return aireDeJeu;
	}

	/**
	 * Retourne les joueurs participants
	 *
	 * @return joueurs participants sous la forme d'une liste de joueurs
	 */
	public ArrayList<Joueur> getJoueurs(){
		return joueurs;
	}

	/**
	 * Ajoute une carte aux joueurs
	 */
	private void suddenDeath() {
		for(Joueur j : joueurs) {
			j.addCarte(pioche());
		}
	}

	/**
	 * Ajoute un joueur aux meilleurs scores
	 *
	 * @param j
	 * 			joueur à ajouter aux meilleurs scores
	 */
	private void ajoutScoreboard(Joueur j) {
		try {
			if (!scoreboard.exists()) {
				scoreboard.createNewFile();
			}
			BufferedReader br = new BufferedReader(new FileReader(scoreboard)); 
			BufferedWriter writer = new BufferedWriter(new FileWriter(scoreboard)); 
			if (br.readLine() == null) {
				writer.write(j.getScore()+"%"+j.getAge()+"%"+j.getNom());
				writer.newLine();
				writer.close();
			} else {
				List<String> lines = Files.readAllLines(scoreboard.toPath());
				int i = 0;
				while(i < lines.size() && Integer.parseInt(lines.get(i).split("%")[0]) < j.getScore()) {
					i++;
				}
				if(i==lines.size()-1)
					i++;
				lines.add(i, j.getScore()+"%"+j.getAge()+"%"+j.getNom());
				for(String s : lines) {
					writer.write(s);
					writer.newLine();
				}
				writer.close();
			}
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Le fichier n'existe pas. Vérifiez la présence du dossier 'files' dans /data/");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
	}

	/**
	 * Sérialisation des données
	 */
	public void save() {
		try {
			if (!savefile.exists())
				savefile.createNewFile();
			ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(savefile)) ;
			oos.writeObject(this);
			oos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

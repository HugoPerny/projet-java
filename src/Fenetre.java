import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class Fenetre implements Serializable {
	
	private JFrame frame;
	private JPanel jeu, main;
	private Plateau plateau;
	private JButton choix;

	/**
	 * Crée la fenêtre principale
	 *
	 * @param j
	 * 			nom du jeu
	 * @param p
	 * 			plateau de jeu lié
	 */
	public Fenetre(String j, Plateau p){
		frame = new JFrame(j);
		jeu = new JPanel(new GridBagLayout());
		main = new JPanel(new GridBagLayout());
		plateau = p;
		
		genererAireDeJeu();
		genererMain();
		
		frame.setLayout(new GridLayout(2,1));
		frame.getContentPane().add(jeu);
		frame.getContentPane().add(main);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setPreferredSize(new Dimension(1024, 768));
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		JOptionPane.showMessageDialog(frame, p.getJoueurs().get(0).getNom()+" commence !");
		
	}

	/**
	 * Permet de redimensionner les cartes
	 *
	 * @param s
	 * 			image à redimensionner
	 */
	private ImageIcon resize(String s) {
		ImageIcon imageIcon = new ImageIcon(s); // load the image to a imageIcon
		Image image = imageIcon.getImage(); // transform it 
		Image newimg = image.getScaledInstance(167, 250,  Image.SCALE_SMOOTH); // scale it the smooth way  
		imageIcon = new ImageIcon(newimg);  // transform it back
		return imageIcon;
	}

	/**
	 * Crée une carte face recto (dans la main)
	 *
	 * @param carte
	 * 				carte à représenter
	 * @return la carte sous forme de JButton
	 */
	private JButton createCarteRecto(Carte carte) {
		JButton c = new JButton(resize("data/"+carte.getRecto()));
		c.setName(carte.getNom());
		c.setMargin(new Insets(0,0,0,0));
		c.setBorder(null);
		c.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				if(choix!=null) 
					choix.setBorder(null);
				c.setBorder(new LineBorder(Color.BLACK));
				choix=c;
			}
		});
		return c;
	}

	/**
	 * Crée une carte face verso (sur le jeu)
	 *
	 * @param carte
	 * 				carte à représenter
	 * @return la carte sous forme de JButton
	 */
	private JButton createCarteVerso(Carte carte) {
		JButton button = new JButton(resize("data/"+carte.getVerso()));
		button.setMargin(new Insets(0,0,0,0));
		button.setBorder(null);
		return button;
	}

	/**
	 * Crée un bouton entre utilisé entre les cartes de l'aire de jeu
	 * 
	 * @param i
	 * 			indice à afficher dans le bouton
	 * @return un bouton sous forme de JButton
	 */
	private JButton createButtonPlacement(int i) {
		JButton button = new JButton(i+"");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (choix==null) {
					JOptionPane.showMessageDialog(frame, "Choisissez d'abord une carte à placer !");
				} else {
					if(plateau.placer(choix.getName(), Integer.parseInt(((JButton)e.getSource()).getText()))) {
						JOptionPane.showMessageDialog(frame, "La carte a bien été placée !");
					} else {
						JOptionPane.showMessageDialog(frame, "La carte a mal été placée !");
					}
					plateau.tourSuivant();
				}
			}
		});
		return button;
	}

	/**
	 * Génère l'aire de jeu
	 */
	private void genererAireDeJeu() {
		int i = 0;
		while (i<plateau.getAireDeJeu().size()) {
			jeu.add(createButtonPlacement(i+1));
			jeu.add(createCarteVerso(plateau.getAireDeJeu().get(i)));
			i++;
		}
		jeu.add(createButtonPlacement(i+1));
	}

	/**
	 * Génère la main
	 */
	private void genererMain() {
		for (Carte carte : plateau.getJoueurs().get(0).getCartes()) {
			main.add(createCarteRecto(carte));
		}
	}

	/**
	 * Met à jour la fenêtre notamment lors d'un changement de tour
	 *
	 * @param n
	 * 			nom du prochain joueur à jouer
	 */
	public void updateFrame(String n) {
		choix=null;
		jeu.removeAll();
		main.removeAll();
		genererAireDeJeu();
		genererMain();
		frame.revalidate();
		frame.repaint();
		JOptionPane.showMessageDialog(frame, "C'est à "+n+" de jouer !");
	}

	/**
	 * Afficher les popups de fin de jeu
	 *
	 * @param n
	 * 			nom du joueur vainqueur
	 */
	public void finJeu(String n) {
		JOptionPane.showMessageDialog(frame, n+" a gagné !");
		String res = "Tableau des meilleurs participants\n\n";
		res += "Nom        Age        Score\n\n";
		try {
			BufferedReader br = new BufferedReader(new FileReader(plateau.scoreboard));
			for(String s : Files.readAllLines(plateau.scoreboard.toPath())) {
				String[] split = s.split("%");
				res+=split[2]+"        "+split[1]+"        "+split[0]+"\n";
			}
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JOptionPane.showMessageDialog(frame, res);
		System.exit(1);
	}
	
}

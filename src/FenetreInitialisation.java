import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FenetreInitialisation {
	
	private Plateau p;
	private JFrame frame;
	private JPanel main;
	private ArrayList<Joueur> joueurs = new ArrayList<Joueur>();

	/**
	 * Crée les fenêtres d'initilisation du jeu
	 *
	 * @param plateau
	 * 					plateau qui va bénéficier des informations
	 */
	public FenetreInitialisation(Plateau plateau){
		p=plateau;
		fenetreInitiale();
	}
	
	/**
	 * Crée la première fenêtre d'initilisation
	 */
	private void fenetreInitiale() {
		frame = new JFrame("Initialisation");
		frame.setLayout(new GridLayout(2,1));
		main = new JPanel(new GridBagLayout());
		JLabel label = new JLabel("Nombre de joueurs ?   ");
		String[] choix = {"2","3","4","5","6"};
		JComboBox<String> combo = new JComboBox<String>(choix);

		main.add(label);
		main.add(combo);
		frame.getContentPane().add(main);
		
		JButton button = new JButton("Ok");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int nb = Integer.parseInt((String)combo.getSelectedItem());
				frame.setVisible(false);
				frame.dispose();
				fenetreJoueurs(nb);
			}
		});
		frame.add(button);
		
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setPreferredSize(new Dimension(200, 100));
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	/**
	 * Crée la seconde fenêtre d'initialisation
	 *
	 * @param nb
	 * 				nombre de joueurs participants
	 */
	private void fenetreJoueurs(int nb) {
		frame = new JFrame("Initialisation");
		frame.setLayout(new BoxLayout(frame.getContentPane(),BoxLayout.Y_AXIS));
		main = new JPanel(new GridLayout(nb*2,2));
		for(int i=0; i<nb;i++) {
			JLabel label = new JLabel("Nom du joueur "+(i+1)+"   ");
			JTextField text = new JTextField();
			text.setName("nom"+i);
			main.add(label);
			main.add(text);
			label = new JLabel("Age du joueur "+(i+1)+"   ");
			text = new JTextField();
			text.setName("age"+i);
			main.add(label);
			main.add(text);
		}
		frame.getContentPane().add(main);

		JPanel ok = new JPanel(new GridLayout(1,1));
		JButton button = new JButton("Ok");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				joueurs = new ArrayList<Joueur>();
				ArrayList<JTextField> textfields = new ArrayList<JTextField>();
				Joueur joueur;
				for(Component c : main.getComponents()) {
					if (c instanceof JTextField) {
						textfields.add((JTextField)c);
					}
				}
				for(int i=0; i<textfields.size()/2; i++) {
					boolean trouve=false;
					int j=0;
					String nom = "", age = "";
					while(!trouve) {
						if(textfields.get(j).getName().equals("nom"+i)) {
							nom=textfields.get(j).getText();
							if(!age.equals(""))
								trouve=true;
						}
						if(textfields.get(j).getName().equals("age"+i)) {
							age=textfields.get(j).getText();
							if(!nom.equals(""))
								trouve=true;
						}
						j++;
					}
					joueurs.add(new Joueur(nom,Integer.parseInt(age)));
				}
				p.initilisationJoueurs(joueurs);
				frame.setVisible(false);
				frame.dispose();
			}
		});
		ok.add(button);
		ok.setPreferredSize(new Dimension(100,20));

		frame.getContentPane().add(ok);
		
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//frame.setPreferredSize(new Dimension(500, 500));
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}

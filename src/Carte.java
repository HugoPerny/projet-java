import java.io.Serializable;

public class Carte implements Serializable {

	private String recto, verso;
	private String nom;
	private int date;

	/**
	 * Crée une carte
	 *
	 * @param jeu
	 * 				timeline ou cardline (ici uniquement timeline) pour permettre de chercher les fichiers dans le bon répertoire
	 * @param data
	 * 				chaine de caractères regroupant le nom, la date et le nom de l'image
	 */
	public Carte(String jeu, String data){
		String[] d = data.split(";");
		this.nom = d[0];
		this.date = Integer.parseInt(d[1]);
		//this.recto = new File(jeu+"/cards/"+d[2]+".jpeg");
		//this.verso = new File(jeu+"/cards/"+d[2]+"_date.jpeg");
		this.recto = jeu+"/cards/"+d[2]+".jpeg";
		this.verso = jeu+"/cards/"+d[2]+"_date.jpeg";
	}
	
	/**
	 * Retourne la date d'une carte
	 *
	 * @return date d'une carte sous la forme d'un entier
	 */
	public int getDate(){
		return date;
	}

	/**
	 * Retourne le recto de la carte
	 *
	 * @return recto de la carte sous forme de chaine de caractères
	 */
	public String getRecto() {
		return recto;
	}

	/**
	 * Retourne le verso de la carte
	 * 
	 * @return verso de la carte sous forme de chaine de caractères
	 */
	public String getVerso() {
		return verso;
	}

	/**
	 * Retourne le nom de la carte
	 *
	 * @return nom de la carte sous forme de chaine de caractères
	 */
	public String getNom() {
		return nom;
	}
	
}
